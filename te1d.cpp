#include <iostream>
#include <vector>
#include <random>

#include <vexcl/vexcl.hpp>
#include <boost/program_options.hpp>

#include "te_system.hpp"

//---------------------------------------------------------------------------
int main(int argc, char *argv[]) {
    // Command line options:
    namespace po = boost::program_options;
    po::options_description desc("Options");
    desc.add_options()
        ("help,h", "Show this help.")
        ("m",    po::value<int>()->default_value(32))
        ("nu",   po::value<double>()->default_value(0.5 * (sqrt(5) - 1), "0.618"))
        ("I",    po::value<double>()->default_value(0.5,    "0.5"))
        ("Kc",   po::value<double>()->default_value(0.0462, "0.0462"))
        ("K",    po::value<double>()->default_value(2.6,    "2.6"))
        ("T",    po::value<double>()->default_value(0.11,   "0.11"))
        ("Edc",  po::value<double>()->default_value(4e-4,   "4e-4"))
        ("eta",  po::value<double>()->default_value(0.02,   "0.02"))
        ("tmax", po::value<double>()->default_value(1000,   "1000"))
        ("dt",   po::value<double>()->default_value(0.02,   "0.02"))
        ;

    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, desc), vm);
    po::notify(vm);

    if (vm.count("help")) {
        std::cout << desc << std::endl;
        return 0;
    }

    const int    m    = vm["m"].as<int>();
    const double nu   = vm["nu"].as<double>();
    const double I    = vm["I"].as<double>();
    const double Kc   = vm["Kc"].as<double>();
    const double K    = vm["K"].as<double>() * Kc;
    const double T    = vm["T"].as<double>() * Kc;
    const double Edc  = vm["Edc"].as<double>();
    const double eta  = vm["eta"].as<double>();
    const double tmax = vm["tmax"].as<double>();
    const double dt   = vm["dt"].as<double>();

    double L = 2 * m * M_PI;
    int n = m * nu;

    // GPU context:
    vex::Context ctx(vex::Filter::Env && vex::Filter::Count(1));
    std::cout << ctx << std::endl;

    std::mt19937 gen;
    std::uniform_int_distribution<size_t> seed;
    vex::Reductor<double> sum(ctx);

    // Initial state:
    state_type S(ctx, n);
    {
        double h = L / n;
        S(0) = vex::element_index() * h;
        S(1) = 0;
    }

    Stepper stepper;
    te_system sys(ctx, n, L, I, K, Edc, eta, T);

    double mean_p2 = 0.0, mean_vx = 0.0;
    int    np = 0;
    int counter = 0;

    for(double t = 0; t < tmax; t += dt) {
        stepper.do_step(std::ref(sys), S, t, dt);
        sys.fluctuate(S, dt, seed(gen));
	counter += sys.counter(S);

        mean_p2 += sum(S(1) * S(1));
        mean_vx += sum(S(1)) / n;
        np += n;
    }

    mean_p2 /= np;
    mean_vx /= np;

    std::cout << "<count> = " << counter << std::endl;
    std::cout << "<vx> = " << mean_vx << std::endl;
    //std::cout << "H = " << sys.hamiltonian(S) << std::endl;
}
