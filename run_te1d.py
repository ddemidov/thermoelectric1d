#!/usr/bin/env python3
import sys
import argparse
import pyte1d
from pylab import *
from tqdm import tqdm

# --- Command line parameters -----------------------------------------------
parser = argparse.ArgumentParser(sys.argv[0])
parser.add_argument('--gpu', dest='gpu', default='')
parser.add_argument('--m',   dest='m',   default=32,     type=int)
parser.add_argument('--nu',  dest='nu',  default=0.5*(5**0.5-1), type=float)
parser.add_argument('--I',   dest='I',   default=0.5,    type=float)
parser.add_argument('--K',   dest='K',   default=2.6,    type=float)
parser.add_argument('--T',   dest='T',   default=0.1,    type=float)
parser.add_argument('--Edc', dest='Edc', default=4e-4,   type=float)
parser.add_argument('--eta', dest='eta', default=0.02,   type=float)
parser.add_argument('--nt',  dest='nt',  default=1000,   type=int)
parser.add_argument('--dt',  dest='dt',  default=0.001,   type=float)
args = parser.parse_args(sys.argv[1:])

# --- Initialize GPU context ------------------------------------------------
pyte1d.context(args.gpu)

# ---------------------------------------------------------------------------
period = 2 * pi
L = period * args.m
n = int(args.nu * args.m)

def potential(x):
    return 0.4*sin(2*x)+sin(x)

c = linspace(0, L, n+1)[:-1]
ss = linspace(0, L, 1000+1)[:-1]
p = zeros_like(c)
yy = zeros_like(c)
ys = potential(ss)
color = sin(c)

ode = pyte1d.thermoelectric(n, L, args.I, args.K, args.Edc, args.eta, args.T)

# --- Integrate over time ---------------------------------------------------
figure(figsize=(25,6))
ax = subplot(111)
time = 0
mean_vx = 0
for i in tqdm(range(args.nt)):
    yy = potential(c)
    ode.advance(c, p, 50 , args.dt)

    mean_vx += mean(p)

    ax.clear()
    ax.set_xlim([0,L])
    ax.set_ylim([-3,3])
    #ax.plot(c,yy, 'o')
    ax.scatter(c,yy,s=100, c = color)
    ax.plot(ss,ys, c = 'black')
    pause(0.01)

mean_vx /= args.nt

print('<vx> = {}'.format(mean_vx))
